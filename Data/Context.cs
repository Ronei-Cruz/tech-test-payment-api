using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Data
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
                .HasOne<Produto>(p => p.Produto)
                .WithMany(p => p.Vendas)
                .HasForeignKey(p => p.ProdutoId);

            modelBuilder.Entity<Venda>()
                .HasOne<Vendedor>(v => v.Vendedor)
                .WithMany(p => p.Vendas)
                .HasForeignKey(v => v.VendedorId);
        }
    }
    
}