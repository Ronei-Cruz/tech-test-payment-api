using AutoMapper;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Mapper
{
    public class Mapeador : Profile
    {
        public Mapeador()
        {
            CreateMap<Vendedor, VendedorDTO>();
            CreateMap<Vendedor, VendedorVendasDTO>();
            CreateMap<Produto, ProdutoDTO>();
            CreateMap<Produto, ProdutoVendasDTO>();
            CreateMap<Venda, VendaDTO>();
            CreateMap<Venda, VendaUpDTO>();
            CreateMap<Venda, VendaStatusDTO>();
        }
    }
}