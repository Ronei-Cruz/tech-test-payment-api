using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Produto
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public List<Venda> Vendas { get; set; }

    }
}