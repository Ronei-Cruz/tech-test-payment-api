using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Enum;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int Id { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public int ProdutoId { get; set; }
        public Produto Produto { get; set; }
        public decimal ValorVenda { get; set; }
        public DateTime DataDaVenda { get; set; }
        public EnumStatusVenda Status { get; set; }
    }
}