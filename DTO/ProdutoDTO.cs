namespace tech_test_payment_api.DTO
{
    public class ProdutoDTO
    {
        public int Id { get; private set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
    }

    public class ProdutoVendasDTO
    {
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public List<int> Vendas { get; set; }
    }
}