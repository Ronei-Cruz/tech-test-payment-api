using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.DTO
{
    public class VendedorDTO
    {
        public int Id { get; private set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public long Telefone { get; set; }
    }

    public class VendedorVendasDTO
    {
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public long Telefone { get; set; }
        public List<int> Vendas { get; set; }
    }
}