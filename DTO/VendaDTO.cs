using tech_test_payment_api.Enum;

namespace tech_test_payment_api.DTO
{
    public class VendaDTO
    {
        public int Id { get; private set; }
        public int VendedorId { get; set; }
        public int ProdutoId { get; set; }
        public decimal ValorVenda { get; set; }
        public DateTime DataDaVenda { get; set; }
        public EnumStatusVenda Status { get; set; }
    }

    public class VendaUpDTO
    {
        public string VendedorNome { get; set; }
        public string ProdutoNome { get; set; }
        public EnumStatusVenda Status { get; set; }
        public DateTime DataDaVenda { get; set; }
    }

    public class VendaStatusDTO
    {
        public EnumStatusVenda Status { get; set; }
    }
}