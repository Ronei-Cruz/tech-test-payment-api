using tech_test_payment_api.Data;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendaServices
    {
        private readonly Context _context;

        public VendaServices(Context context)
        {
            _context = context;
        }

        public void AddVenda(VendaDTO vendaDTO)
        {
            var venda = new Venda()
            {
                VendedorId = vendaDTO.VendedorId,
                ProdutoId = vendaDTO.ProdutoId,
                ValorVenda = vendaDTO.ValorVenda,
                DataDaVenda = DateTime.Now
            };
            _context.Vendas.Add(venda);
            _context.SaveChanges();
        }

        public List<Venda> GetVendas()
        {
            var listaVendas = _context.Vendas.ToList();
            return listaVendas;
        }

        public VendaUpDTO GetVenda(int id)
        {
            var venda = _context.Vendas.Where(v => v.Id == id).Select(v => new VendaUpDTO()
            {
                VendedorNome = v.Vendedor.Nome,
                ProdutoNome = v.Produto.Nome,
                Status = v.Status,
                DataDaVenda = v.DataDaVenda,
            }).FirstOrDefault();
            return venda;
        }

        public Venda AtualizarVenda(int id, VendaStatusDTO vendaStatusDTO)
        {
            var venda = _context.Vendas.FirstOrDefault(v => v.Id == id);
            if(venda != null)
            {
                venda.Status = vendaStatusDTO.Status;
                
                _context.SaveChanges();
            }
            return venda;
        }

        public void DeleteVenda(int id)
        {
            var venda = _context.Vendas.FirstOrDefault(v => v.Id == id);
            if (venda != null)
            {
                _context.Vendas.Remove(venda);
                _context.SaveChanges();
            }
        }

    }
}