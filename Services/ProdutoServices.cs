using tech_test_payment_api.Data;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class ProdutoServices
    {
        private readonly Context _context;

        public ProdutoServices(Context context)
        {
            _context = context;
        }

        public void AddProduto(ProdutoDTO produtoDTO)
        {
            var produto = new Produto()
            {
                Nome = produtoDTO.Nome,
                Preco = produtoDTO.Preco,
            };

            _context.Produtos.Add(produto);
            _context.SaveChanges();
        }

        public List<Produto> GetProdutos()
        {
            var listaProdutos = _context.Produtos.ToList();
            return listaProdutos;
        }

        public ProdutoVendasDTO GetProduto(int id)
        {
            var produto = _context.Produtos.Where(p => p.Id == id).Select(p => new ProdutoVendasDTO()
            {
                Nome = p.Nome,
                Preco = p.Preco,
                Vendas = p.Vendas.Select(pd => pd.Id).ToList()
            }).FirstOrDefault();

            return produto;
        }

        public Produto AtualizarProduto(int id, ProdutoDTO produtoDTO)
        {
            var produto = _context.Produtos.FirstOrDefault(p => p.Id == id);        
            if(produto != null)
            {
                produto.Nome = produtoDTO.Nome;
                produto.Preco = produtoDTO.Preco;

                _context.SaveChanges();
            }
            
            return produto;
        }

        public void DeleteProduto(int id)
        {
            var produto = _context.Produtos.FirstOrDefault(p => p.Id == id);
            if(produto != null)
            {
                _context.Produtos.Remove(produto);
                _context.SaveChanges();
            }
        }
    }
}