using tech_test_payment_api.Data;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendedorServices
    {
        private readonly Context _context;

        public VendedorServices(Context context)
        {
            _context = context;
        }

        public void AddVendedor(VendedorDTO vendedorDTO)
        { 
            var vendedor = new Vendedor()
            {
                Nome = vendedorDTO.Nome,
                Cpf = vendedorDTO.Cpf,
                Email = vendedorDTO.Email,
                Telefone = vendedorDTO.Telefone
            };

            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
        }

        public List<Vendedor> GetVendedores()
        {
            var listaVendedores = _context.Vendedores.ToList();
            return listaVendedores;
        }

        public VendedorVendasDTO GetVendedor(int id)
        {
            var vendedor = _context.Vendedores.Where(v => v.Id == id).Select(v => new VendedorVendasDTO()
            {
                Nome = v.Nome,
                Cpf = v.Cpf,
                Email = v.Email,
                Telefone = v.Telefone,
                Vendas = v.Vendas.Select(pd => pd.Id).ToList()
        }).FirstOrDefault();
            return vendedor;
        }

        public Vendedor AtualizarVendedor(int id, VendedorDTO vendedorDTO)
        {
            var vendedor = _context.Vendedores.FirstOrDefault(v => v.Id == id);
            if(vendedor != null)
            {
                vendedor.Nome = vendedorDTO.Nome;
                vendedor.Cpf = vendedorDTO.Cpf;
                vendedor.Email = vendedorDTO.Email;
                vendedor.Telefone = vendedorDTO.Telefone;

                _context.SaveChanges();
            }
            return vendedor;
        }

        public void DeleteVendedor(int id)
        {
            var vendedor = _context.Vendedores.FirstOrDefault(v => v.Id == id);
            if (vendedor != null)
            {
                _context.Vendedores.Remove(vendedor);
                _context.SaveChanges();
            }
        }
    }
}