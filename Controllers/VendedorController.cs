using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendedorServices _vendedorServices;
        
        private readonly IMapper _mapper;

        public VendedorController(VendedorServices vendedorServices, IMapper mapper)
        {
            _vendedorServices = vendedorServices;
            _mapper = mapper;
        }

        [HttpPost("add-vendedor")]
        public IActionResult AddVendedor(VendedorDTO vendedorDTO)
        {
            if (!ModelState.IsValid)
                return BadRequest(new {Message = "Verifique o campo e tente novamente" });

            _vendedorServices.AddVendedor(vendedorDTO);
            return Ok(new { Message = "Vendedor salvo com sucesso!" });
        }

        [HttpGet("list-vendedores")]
        public IActionResult GetVendedores()
        {
            if (!ModelState.IsValid)
                return BadRequest(new {Message = "Verifique o campo e tente novamente" });

            var listaVendedores = _mapper.Map<List<VendedorDTO>>(_vendedorServices.GetVendedores());
            return Ok(listaVendedores);
        }

        [HttpGet("{id}")]
        public IActionResult GetVendedor(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            var vendedor = _vendedorServices.GetVendedor(id);

            if(vendedor == null)
                return BadRequest(new { Message = "Vendedor não encontrado." });

            return Ok(vendedor);
        }

        [HttpPut("update-vendedor/{id}")]
        public IActionResult AtualizarVendedor(int id, VendedorDTO vendedorDTO)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente" });

            var atualizar = _mapper.Map<List<VendedorDTO>>(_vendedorServices.AtualizarVendedor(id, vendedorDTO));

            if(atualizar == null)
                return BadRequest(new { Message = "Vendedor não encontrado." });

            return Ok(atualizar);
        }

        [HttpDelete("del-vendedor/{id}")]
        public IActionResult DeletarVendedor(int id)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });
            _vendedorServices.DeleteVendedor(id);
            return Ok(new { Message = "Vendedor deletado com sucesso!" });
        }
    }
}