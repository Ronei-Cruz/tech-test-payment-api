using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly ProdutoServices _produtoServices;
        private readonly IMapper _mapper;

        public ProdutoController(ProdutoServices produtoServices, IMapper mapper)
        {
            _produtoServices = produtoServices;
            _mapper = mapper;
        }

        [HttpPost("add-produto")]
        public IActionResult AddProduto(ProdutoDTO produtoDTO)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            _produtoServices.AddProduto(produtoDTO);
            return Ok(new { Message = "Produto salvo com sucesso!" });
        }

        [HttpGet("list-produto")]
        public IActionResult GetProdutos()
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente" });

            var listaProdutos = _mapper.Map<List<ProdutoDTO>>(_produtoServices.GetProdutos());
            return Ok(listaProdutos);
        }

        [HttpGet("{id}")]
        public IActionResult GetProduto(int id)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            var produto = _produtoServices.GetProduto(id);

            if(produto == null)
                return BadRequest(new { Message = "Produto não encontrado." });

            return Ok(produto);
        }

        [HttpPut("update-produto/{id}")]
        public IActionResult AtualizarProduto(int id, ProdutoDTO produtoDTO)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente" });

            var atualizar = _mapper.Map<List<ProdutoDTO>>(_produtoServices.AtualizarProduto(id, produtoDTO));

            if(atualizar == null)
                return BadRequest(new { Message = "Produto não encontrado." });

            return Ok(atualizar);
        }

        [HttpDelete("del-produto/{id}")]
        public IActionResult DeletarProduto(int id)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });
            _produtoServices.DeleteProduto(id);
            return Ok(new { Message = "Produto deletado com sucesso!" });
        }
    }
}