using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaServices _vendaService;
        private readonly IMapper _mapper;

        public VendaController(VendaServices vendaServices, IMapper mapper)
        {
            _vendaService = vendaServices;
            _mapper = mapper;
        }

        [HttpPost("add-vendas")]
        public IActionResult AddVendas(VendaDTO vendaDTO)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            _vendaService.AddVenda(vendaDTO);
            return Ok(new { Message = "Venda feita com sucesso."});
        }

        [HttpGet("list-vendas")]
        public IActionResult GetVendas()
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            var listaVendas = _mapper.Map<List<VendaDTO>>(_vendaService.GetVendas());
            return Ok(listaVendas);
        }

        [HttpGet("{id}")]
        public IActionResult GetVenda(int id)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            var venda = _vendaService.GetVenda(id);
            if(venda == null)
                return BadRequest(new { Message = "Venda não encontrada." });

            return Ok(venda);
        }

        [HttpPut("update-venda/{id}")]
        public IActionResult AtualizarVeda(int id, VendaStatusDTO vendaStatusDTO)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente" });

            var atualizar = _mapper.Map<List<VendaStatusDTO>>(_vendaService.AtualizarVenda(id, vendaStatusDTO));

            if(atualizar == null)
                return BadRequest(new { Message = "Vendedor não encontrado." });

            return Ok(atualizar);
        }

        [HttpDelete("del-venda/{id}")]
        public IActionResult DeleteVenda(int id)
        {
            if(!ModelState.IsValid)
                return BadRequest(new { Message = "Verifique o campo e tente novamente." });

            _vendaService.DeleteVenda(id);

            return Ok(new { Message = "Venda deletada com sucesso."});
        }
    }
}